<?php

return[
	'allpages' =>'allpages',

    'positions'=>[
        'component-name' => '--By Component Name--',
        'showcase' => 'showcase',
        'content-header' => 'content-header',
        'content-footer' => 'content-footer',
        'page-bottom' => 'page-bottom'
    ],
    
    'types'=>[
        'page-images'=>[
            'name'=>'Page Image Viewer',
            'type'=>'page-images',
            'view'=>'Component::page-images',
            'position'=>'content-header',
            'detachable_by'=>'component.allow-detach',
            'selectable'=>false,
            'default'=>[],
        ],
        'banner-slideshow'=>[
            'name'=>'Images Block',
            'type'=>'banner-slideshow',
            'view'=>'Component::banner-slideshow',
            'position'=>'content-header',
            'detachable_by'=>'component.allow-detach',
            'selectable'=>true,
            'default'=>[],
            'composer'=>[],
            'update-trigger'=>'
                app("MediaHandler")->updateLink(isset($componentData["medias"])?$componentData["medias"]:[],$component);
            ',
            'delete-trigger'=>'
                app("MediaHandler")->deleteLinkFrom($component);
            ',
        ],
        'custom-form'=>[
            'name'=>'Custom Form',
            'type'=>'custom-form',
            'view'=>'Component::custom-form',
            'position'=>'content-footer',
            'detachable_by'=>'component.allow-detach-code',
            'editable_by'=>'superuser',
            'compilers'=>['html'=>'html', 'blade'=>'Blade', 'php'=>'PHP'],
            'selectable'=>true,
            'update-trigger'=>'
                if ($component->getParams("admin_save","")){
                    eval($component->getParams("admin_save",""));
                }
            ',
            'default'=>[
                'compiler'=>'blade',
                'content'=>"
				{!! view('public::notifications')!!}
				{!!Form::vertical_open()->id('contact-form')
			                    ->action(url('component/'.\$component->id))
			                    ->method('POST')
			                    ->class('white-row')!!}
                
{!! Form::text('name')->label('Name: ')-> placeholder('Enter your name.')!!}
{!! Form::email('email')->label('Email : ')-> placeholder('Enter your email.')!!}
                
                
{!! Form::textarea('msg')->label('Message: ')-> placeholder('Enter your enquiries.')!!}
{!! Captcha::render() !!}
                
{!! Form::hidden('return')->value(Request::url())!!}
{!! Form::submit('Submit')->class('btn btn-primary')!!}
{!! Form::close() !!}",
                'validate_json'=>'{"name":"required|max:255","email":"required|email|max:255","msg":"required","g-recaptcha-response":"required|recaptcha"}',
                'submit_execute'=>'$body = htmlentities($request->get("msg"))
 . "\n\n From: ".$request->get("name")
 . "\n Email: ".$request->get("email");
flash("Thank you for your enquires, we will get back to you within 2 business days.");
sendPlainMail("Online Enquiries",$body)->log("contact-us", $request->only(["name", "email", "msg"]));',
            ],
        ],
        'custom-html'=>[
            'name'=>'Text Block',
            'type'=>'custom-html',
            'view'=>'Component::custom-html',
            'position'=>'content-header',
            'detachable_by'=>'component.allow-detach',
            'compilers'=>['html'=>'html','text'=>'Text', 'blade'=>'Blade', 'php'=>'PHP'],
            'selectable'=>true,
            'default'=>[
                'compiler'=>'blade',
                'content'=>'<h1>Your content here..</h1>',
            ],
            
        ],
        'module-viewer'=>[
            'name'=>'Module Viewer',
            'type'=>'module-viewer',
            'view'=>'Component::module-viewer',
            'position'=>'content-header',
            'detachable_by'=>'component.allow-detach-code',
            'editable_by'=>'superuser',
            'selectable'=>true,
            'default'=>[
                'view'=>'Product::detail',
                'preload_php'=>'$view->product = \App\Modules\Product\Models\Product::getProdFromSlug($slug);'
            ],
            
        ],
    ]
];