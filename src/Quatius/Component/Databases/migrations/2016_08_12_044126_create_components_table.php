<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',50);
            $table->string('name',150);
            $table->text('data');
            $table->string('editable_by')->nullable()->default("");
            $table->string('detachable_by')->nullable()->default("component.allow-detach-code");
            $table->boolean('published')->nullable()->default(1);
            $table->dateTime('publish_start')->nullable();
            $table->dateTime('publish_end')->nullable();
            $table->timestamps();
        });
        
        Schema::create('component_placements', function (Blueprint $table) {
        	
        	$table->increments('id');
        	$table->integer('component_id')->unsigned();
        	
        	$table->string('url', 150)->default('');
        	$table->string('position', 50)->default('content-footer');
        	$table->integer('ordering')->default(0);
        	
        	$table->foreign('component_id')
        		->references('id')
        		->on('components')
        		->onDelete('cascade');
        	
        	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::drop('component_placements');
        Schema::drop('components');
    }
}
