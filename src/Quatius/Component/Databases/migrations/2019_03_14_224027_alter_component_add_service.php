<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComponentAddService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('components', function(Blueprint $table){
            $table->text('register_service')->nullable()->after('data');
        });
        
        try{
            DB::table('permissions')->insert([
                [
                    'slug'  => 'component.set-position',
                    'name'   => 'Set Position',
                ]   ,
                [
                    'slug'  =>'component.allow-detach',
                    'name'  => 'Allow Detach'
                ],
                [
                    'slug'  =>'component.set-publish',
                    'name'  => 'Set Visibility'
                ],
                [
                    'slug'  =>'component.select-existing',
                    'name'  =>  'Select From Existing'
                ],
                [
                    'slug'  =>'component.view-test',
                    'name'  => 'View Test Mode'
                ],
            ]);
        }catch (Exception $e){

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('components', function(Blueprint $table){
            $table->dropColumn('register_service');
        });
    }
}
