<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterComponentPlacementsAddMorphBinding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('component_placements', function(Blueprint $table){
            $table->string('url', 150)->nullable()->change();
            $table->integer('component_placement_id')->unsigned()->nullable()->after('url');
            $table->string('component_placement_type',255)->nullable()->after('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('component_placements', function(Blueprint $table){
            $table->dropColumn('component_placement_id');
            $table->dropColumn('component_placement_type');
            $table->string('url', 150)->change();
        });
    }
}
