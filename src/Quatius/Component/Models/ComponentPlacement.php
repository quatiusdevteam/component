<?php namespace Quatius\Component\Models;

use Illuminate\Database\Eloquent\Model;

class ComponentPlacement extends Model {
	
	protected $fillable = [
		'url',
	    'component_placement_type',
	    'component_placement_id',
	 	'position',
		'ordering',
	];
	
	public function getComponent($urlData=null)
	{
		$component = $this->component;
		
		if(!$component)
			$component = new Component();
		
		$component->setUrlData($urlData);
		return $component;
	}
	
	public function component(){
		return $this->belongsTo(Component::class);
	}
}
