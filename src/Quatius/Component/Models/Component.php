<?php namespace Quatius\Component\Models;

use Theme;
use Illuminate\Database\Eloquent\Model;
use View;
use Lavalite\Page\Models\Page;
use Quatius\Framework\Traits\ParamSetter;

class Component extends Model {
	
	use ParamSetter;
	
	protected $fillable = [
		'type',
		'name',
		'data',
		'register_service',
		'editable_by',
		'detachable_by',
		'publish_start',
		'publish_end',
	    'published'
	];
	
	protected $attributes  = array(
	    'published' => 1,
	);
	
	protected $url_data = null;
	
	private $dataParams = null;
	 
	public static function boot()
	{
	    parent::boot();
	    
	    static::deleted(function($component){
	        
	        $triggerDelete= config('quatius.component.types.'.$component->type.'.delete-trigger','');
	        if ($triggerDelete != ''){
	            try {
	                eval($triggerDelete);
	            } catch (\Exception $e) {
	                app('log')->error('Component "'.$component->type.' '.$component->id.' deleting.');
	            }
	        }
	    });
	}
	
	public function getAdminView($view = 'view', $override_root='admin::components'){
		if ($view == 'edit')
		{
			if ($this->editable_by != "")
			{
				if (!(user('admin.web')->hasRole('superuser') || user('admin.web')->hasRole($this->editable_by)))
				{
					if (View::exists(config('quatius.component.types.'.$this->type.'.view').'.admin.'.$view.'-readonly'))
						$view .='-readonly';
					else 
						$view = 'view';
				}
			}
		}
		$view_override = $override_root.'.'.$this->type.'.admin.'.$view;
		
		if(View::exists($view_override)){
		  return $view_override;
		}
		
		return config('quatius.component.types.'.$this->type.'.view').'.admin.'.$view;
	}
	
	public function getUrlData(){
		if ($this->url_data == null)
			$this->url_data = ['page' => new Page(), 'param' => ''];
			
		return $this->url_data;	
	}
	
	public function setUrlData($value){
		$this->url_data = $value;
	}
	
	public function getData(){
		if ($this->dataParams == null)
		{
			if ($this->id == 0 && !isset($this->data))
				$this->data = json_encode(config('quatius.component.types.'.$this->type.'.default'));
			
			$this->dataParams = json_decode($this->data);
		}
		return $this->dataParams;
	}
	
	public function hasData($key){
		$datas = $this->getData();
		
		if (isset($datas->$key))
			return true;
		
		return false;
	}
	
	public function placements(){
		return $this->hasMany(ComponentPlacement::class);
	}
	
	public function getView($view = 'view', $override_root='public::components'){
	    
	    $view_override = $override_root.'.'.$this->type.'.'.$view;
	    
	    if(View::exists($view_override)){
	        return $view_override;
	    }
	    
		return config('quatius.component.types.'.$this->type.'.view').'.'.$view;
	}
	
	public function renderView($placement){
		return view('Component::view', ['component'=>$this, 'placement'=>$placement])->render();
	}

	public function configDefault($field, $default=null){
		if ($default===null)
			return config('quatius.component.types.'.$this->type.'.default.'.$field);
		else
			return config('quatius.component.types.'.$this->type.'.default.'.$field, $default);
	}

	public function getDefaultParams()
    {
        return 'data';
	}
	
	public function applyDefault(){
        $this->data = json_encode(config('quatius.component.types.'.$this->type.'.default'));
	}
}
