<?php

namespace Quatius\Component\Repositories\Handlers;

use Quatius\Component\Models\ComponentPlacement;
use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Component\Repositories\ComponentRepository;
use DB;
use Request;

class ComponentHandler extends QuatiusRepository implements ComponentRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.component.component.model', \Quatius\Component\Models\Component::class);
    }

    public function serialize($type, $data)
    {
        $default = config('quatius.component.types.' . $type . '.default');
        $merge = array_merge($default, $data);
        return json_encode($merge);
    }

    private function _getPlacementsByUrl($url = '', $model = null, $publishOnly = true, $isFront = false, $testMode = false)
    {
        $queryPlacement = ComponentPlacement::where(function ($upperQuery) use ($url, $model, $isFront) {

            $page = config('quatius.component.allpages', '')?App('Lavalite\Page\Page')->findWhere(['slug' => config('quatius.component.allpages', '')])->first():null;
            if ($url !== null && $model) {
                $upperQuery->where(function ($query) use ($model, $isFront, $page) {
                    $query->orWhere(function ($query) use ($model) {
                        $query->where('component_placement_id', $model->id)
                            ->where('component_placement_type', get_class($model));
                    });
                    
                    if ($isFront && $page) {
                        $query->orWhere(function ($query) use($page){
                            $query->where('component_placement_id', $page->id)
                                ->where('component_placement_type', get_class($page));
                        });
                    }
                });
            } else if ($model) {
                $upperQuery->where(function ($query) use ($model, $isFront, $page) {
                    $query->Where(function ($query) use ($model) {
                        $query->where('component_placement_id', $model->id)
                            ->where('component_placement_type', get_class($model));
                    });
                    if ($isFront && $page) {
                        $query->orWhere(function ($query) use($page){
                            $query->where('component_placement_id', $page->id)
                                ->where('component_placement_type', get_class($page));
                        });
                    }
                });
            } else {
                
                if (!$page || !$isFront) return collect([]);

                $upperQuery->where(function ($query) use ($page) {
                    $query->where('component_placement_id', $page->id)
                    ->where('component_placement_type', get_class($page));
                });
            }
            
            if ($url !== null)
                $upperQuery->orWhere('url','<>','');
        });

        $queryPlacement->leftJoin(DB::raw('(SELECT published, publish_start, publish_end, id as component_id FROM components) AS compon'), 'component_placements.component_id', 'compon.component_id');
        
        if ($publishOnly) {
            $queryPlacement->where(function ($queryMode) use ($testMode) {
                $queryMode->where(function ($queryPub) {
                    $queryPub->wherePublished(1);
                    $queryPub->where(function ($queryDate) {
                        $queryDate->whereDate('publish_end', '<', date("Y-m-d H:i:s"));
                        $queryDate->orWhereNull('publish_end');
                    });
                });
                if ($testMode) {
                    $queryMode->orWhere('published', 2);
                }
            });
        } else {
            $queryPlacement->whereIn('published', [0, 1, 2]);
        }

        $placements = $queryPlacement->with('component')->orderBy('ordering')->get();

        if ($isFront){
            $placements = $placements->filter(function ($value){
                if (trim($value->url)){
                    return Request::is(trim($value->url));
                }
                return true;
            });
        }
        return $placements;
    }

    public function filterDate($comp)
    {
        return $comp->filter(function ($item) {

            return $item->published == 2 || ((!isset($item->publish_start) || !$item->publish_start)
                || ($item->publish_start && strtotime($item->publish_start) <= strtotime('now')))
                && ((!isset($item->publish_end) || !$item->publish_end)
                    || ($item->publish_end && strtotime($item->publish_end) > strtotime('now')));
        });
    }

    public function _getServices($testMode = false)
    {
        $queryPlacement = $this->makeModel()->where(function ($query) {
            $query->whereNotNull('register_service');
            $query->where('register_service', '!=', '');
        });

        $queryPlacement->where(function ($queryMode) use ($testMode) {
            $queryMode->where(function ($queryPub) {
                $queryPub->wherePublished(1);
                $queryPub->where(function ($queryDate) {
                    $queryDate->whereDate('publish_end', '<', date("Y-m-d H:i:s"));
                    $queryDate->orWhereNull('publish_end');
                });
            });
            if ($testMode) {
                $queryMode->orWhere('published', 2);
            }
        });

        return $queryPlacement->with('placements')->get();
    }

    public function getServices($testMode=false)
    {
        date_default_timezone_set(config("app.timezone", "UTC"));
        if (!$this->allowedCache('getServices') || $this->isSkippedCache()) {
            return $this->filterDate($this->_getServices($testMode));
        }

        $key = $this->getCacheKey('getServices', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($testMode) {
            return $this->_getServices($testMode);
        });

        $this->resetModel();
        $this->resetScope();

        return $this->filterDate($value);
    }

    public function getPlacementsByUrl($url = '', $model = null, $publishOnly = true, $isFront = false, $testMode = false)
    {
        date_default_timezone_set(config("app.timezone", "UTC"));
        if (!$this->allowedCache('getPlacementsByUrl') || $this->isSkippedCache()) {
            $value = $this->_getPlacementsByUrl($url, $model, $publishOnly, $isFront, $testMode);

            if ($publishOnly) {
                $value = $this->filterDate($value);
            }

            return $value;
        }

        $args = func_get_args();
        $args[] = request()->getHttpHost().request()->getBaseUrl().'/'.request()->path();
        $key = $this->getCacheKey('getPlacementsByUrl', $args);

        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($url, $model, $publishOnly, $isFront, $testMode) {
            return $this->_getPlacementsByUrl($url, $model, $publishOnly, $isFront, $testMode);
        });


        $this->resetModel();
        $this->resetScope();

        if ($publishOnly) {
            return $this->filterDate($value);
        }
        return $value;
    }

    public function hasTesting()
    {
        return !!$this->findByField('published', 2)->count();
    }
}
