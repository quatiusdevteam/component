<?php

namespace Quatius\Component\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Quatius\Component\Models\ComponentPlacement;
use Quatius\Component\Repositories\ComponentRepository;
use Illuminate\Database\Eloquent\Model;
use Quatius\Component\Models\Component;
use DB;

class ComponentController extends Controller
{

    protected $repo = null;

    public function __construct(ComponentRepository $repo)
    {
        $this->repo = $repo;

        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.map.admin.theme'), config('theme.map.admin.layout'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $query = DB::table('components')->leftJoin(DB::raw('(SELECT GROUP_CONCAT(id) as placement_ids, GROUP_CONCAT(component_placement_id) as component_placement_id, GROUP_CONCAT(url) as url, component_id FROM component_placements group by component_id) AS placements'),'id', 'placements.component_id');

            $placements = dataTableQuery($request->all(), $query);
            return response()->json($placements, 200);
        }

        return $this->theme->of("Component::admin.index")->render();
    }
    public function submitAdminByType(Request $request, $type, $action = "", $value = "")
    {
        $component = $this->repo->makeModel()->newInstance(config('quatius.component.types.' . $type));
        return $this->renderAdminExecute($request, $component, $action, $value);
    }

    public function submitAdminById(Request $request, $id, $action = "", $value = "")
    {
        $component = Component::find($id);
        return $this->renderAdminExecute($request, $component, $action, $value);
    }

    private function renderAdminExecute(Request $request, $component, $action = "", $value = "")
    {
        $response = "";
        try {
            eval($component->getParams('submit_admin_execute'));
        } catch (Exception $e) {
            if ($request->wantsJson()) {
                return response()->json(
                    [
                        'message' => $e->getMessage(),
                        'code'    => 400,
                    ],
                    400
                );
            }
        }

        if ($request->wantsJson()) {
            if (is_array($response)) {
                return response()->json($response, isset($response['code']) ? $response['code'] : 200);
            } elseif ($response instanceof \Symfony\Component\HttpFoundation\Response || $response instanceof \Illuminate\Contracts\Routing\ResponseFactory) {
                return $response;
            }
            return response()->json(["response" => $response]);
        }

        if ($request->has('return'))
            return redirect($request->get('return'));

        return redirect()->back();
    }

    public function clearUnlinkedComponents(Request $request)
    {
        try {
            $results = DB::table('components')
                        ->leftJoin(DB::raw('(SELECT component_id FROM component_placements group by component_id) AS placements'),'id', 'component_id')
                        ->whereNull('component_id')->where('published','<>', 3)->delete(); // excluded template
            
            if ($results) $this->repo->clearCache();

            return response()->json([
                'message'  => 'Deleted Component(s) :' . $results ,
                'code'     => 204
            ], 201);
           
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }
    }

    public function saveComponents(Request $request, $url, $model = null)
    {
        if (($url === null && !$model) || !$request->has('components'))
            return;

        $components = $request->get("components");

        $compIds = [];

        foreach ($components as $componentData) {
            if ($componentData['id'] == "" || $componentData['id'] == 0) {
                continue;
            }
            $compIds[] = $componentData['id'];
        }

        $loadedComps = $this->repo->makeModel()->whereIn('id', $compIds)->with('placements')->get()->keyBy('id');

        $c = 0;
        foreach ($components as $index => $componentData) {
            $component = null;
            $triggerUpdate = config('quatius.component.types.' . $componentData['type'] . '.update-trigger', '');

            $placement = $componentData['placement'];
            if ($model instanceof Model) {
                $placement['component_placement_type'] =  get_class($model);
                $placement['component_placement_id'] = $model->id;
            }

            $placement['ordering'] = $c;
            if ($placement['position'] == 'component-name')
                $placement['position'] = $componentData['name'];

            unset($componentData['placement']);

            if ($placement['deleted']) {
                if ($componentData['id'] > 0 && $placement['id'] > 0) {
                    ComponentPlacement::find($placement['id'])->delete();
                    $this->repo->clearCache();
                    $component = $loadedComps->get($componentData['id']);
                    if ($component->placements->count() == 0)
                        $component->delete();
                }

                continue;
            } elseif ($componentData['id'] == "" || $componentData['id'] == 0) {
                unset($componentData['id']);
                unset($placement['id']);
                unset($placement['deleted']);

                if (isset($componentData['data']))
                    $componentData['data'] = $this->repo->serialize($componentData['type'], $componentData['data']);

                $component = $this->repo->create($componentData);
                if (isset($componentData['register_service']))
                    $this->repo->update(["register_service" => $componentData['register_service']], $component->id);

                if ($triggerUpdate != '') {
                    try {
                        eval($triggerUpdate);
                    } catch (\Exception $e) {
                        throw new \Exception('Component "' . $componentData['type'] . '" saving error.');
                    }
                }
                $component->placements()->save(new ComponentPlacement($placement));
            } else {
                $component = $loadedComps->get($componentData['id']);
                unset($componentData['id']);
                unset($placement['deleted']);

                if (isset($componentData['data']))
                    $componentData['data'] = $this->repo->serialize($componentData['type'], $componentData['data']);
                $this->repo->update($componentData, $component->id);

                if (isset($componentData['register_service']))
                    $this->repo->update(["register_service" => $componentData['register_service']], $component->id);

                $component->fill($componentData);
                if ($triggerUpdate != '') {
                    try {
                        eval($triggerUpdate);
                    } catch (\Exception $e) {
                        throw new \Exception('Component "' . $componentData['type'] . '" saving error.');
                    }
                }

                if ($placement['id'] == "" || $placement['id'] == 0)
                    $component->placements()->save(new ComponentPlacement($placement));
                else
                    ComponentPlacement::find($placement['id'])->update($placement);
            }

            $c++;
        }

        $this->repo->clearCache();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createView(Request $request, $index, $type)
    {
        $component = $this->repo->makeModel()->newInstance(config('quatius.component.types.' . $type));
        
        $component->applyDefault();
        
        $placement = new ComponentPlacement(['position' => config('quatius.component.types.' . $type . '.position')]);

        $this->theme->layout(config('theme.map.admin.ajax'));

        return $this->theme->of('Component::admin-edit', ['index' => $index, 'placement' => $placement,  'component' => $component])->render();
    }

    public function editComponent(Request $request, $id, $index)
    {
        $component = $this->repo->find($id);
        if ($component->published == 3) {
            $component->published = $component->getParams('init-published', '1');
        }

        $placement = new ComponentPlacement(['position' => config('quatius.component.types.' . $component->type . '.position')]);

        $this->theme->layout(config('theme.map.admin.ajax'));

        return $this->theme->of('Component::admin-edit', ['index' => $index, 'placement' => $placement,  'component' => $component])->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request, $comp_id, $pos_id)
    {
        $component = $this->repo->find($comp_id);
        $placement = $pos_id == 0?new ComponentPlacement() : $component->placements->where('id', $pos_id)->first();
        $index = 0;
        $this->theme->layout(config('theme.map.admin.ajax'));
        return $this->theme->of("Component::admin.show", compact('component','placement', 'index'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $comp_id, $pos_id)
    {
        $component = $this->repo->find($comp_id);
        $placement = $pos_id == 0?new ComponentPlacement() : $component->placements->where('id', $pos_id)->first();
        $index = 0;

        $this->theme->layout(config('theme.map.admin.ajax'));
        return $this->theme->of("Component::admin.edit", compact('component', 'placement', 'index'))->render();
    }
    public function copyTemplate(Request $request, $comp_id, $override_id = 0)
    {
        try {
            $component = $this->repo->find($comp_id);
            $template = $component->replicate();
            $template->name = $request->get('name', $template->name);
            $template->setParams('init-published', $request->get('init', '1'));
            $template->published = 3;
            $template->detachable_by = $request->get('selectable_by', "component.allow-detach");
            $template->publish_start = null;
            $template->publish_end = null;

            $template->name = $request->get('name', $template->name);
            if ($this->repo->count(['id' => $override_id])) {
                $templateNew = $this->repo->update($template->toArray(), $override_id);
                return response()->json([
                    'message'  => 'Updated Template - ' . $template->name,
                    'code'     => 204
                ], 201);
            } else {
                $templateNew = $this->repo->create($template->toArray());
                return response()->json([
                    'new_id' => $templateNew->id,
                    'message'  => 'Create new Template - ' . $template->name,
                    'code'     => 204
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }
    }
    public function removeTemplate(Request $request, $comp_id)
    {
        try {
            $this->repo->delete($comp_id);
            return response()->json([
                'message'  => 'Template Deleted',
                'code'     => 204
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
