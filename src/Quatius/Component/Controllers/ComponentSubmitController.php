<?php namespace Quatius\Component\Controllers;

use Illuminate\Http\Request;
use Quatius\Component\Models\Component;
use Validator;
use App\Http\Controllers\PublicWebController;

class ComponentSubmitController extends PublicWebController {
	
	public function submitForm(Request $request, $id, $action="", $value="")
	{
		$component = Component::find($id);
		$response = "";
		
		if ($request->isMethod('POST') && $component->hasData('validate_json'))
		{
			$validator = Validator::make($request->all(), json_decode($component->getData()->validate_json, true));
			if ($validator->fails()) {
			    return redirect()->back()->withInput($request->input())->withErrors($validator);
			}
		}

		$response = "";
        try{
		    eval($component->getParams('submit_execute'));
        } catch (Exception $e) {
            if ($request->wantsJson()){
                return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
            }
        }

        if ($request->wantsJson()){
            if (is_array($response)){
                return response()->json($response, isset($response['code'])?$response['code']:200);
			}
			elseif($response instanceof \Symfony\Component\HttpFoundation\Response || $response instanceof \Illuminate\Contracts\Routing\ResponseFactory){
                return $response;
            }
            return response()->json(["response"=>$response]);
        }
        
        if($response instanceof \Symfony\Component\HttpFoundation\Response || $response instanceof \Illuminate\Contracts\Routing\ResponseFactory){
            return $response;
        }

		if($request->has('return'))
			return redirect($request->get('return'));
		
		return redirect()->back();
    }
}
