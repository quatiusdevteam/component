<?php

namespace Quatius\Component\Middlewares;

use DB;
use Event;

class GlobalComponents
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string|null $guard    
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        if (array_search('web', $request->route()->middleware()) !== false) {
            // live or test
            config()->set('quatius.component.test_mode', !user('admin.web')->hasRole('guest') && (user('admin.web')->hasRole('superuser') || user('admin.web')->hasPermission('component.view-test')) && app('ComponentHandler')->hasTesting());

            $componentServices = app('ComponentHandler')->getServices(config('quatius.component.test_mode', false));
            
            //get active page ids
            $activePageIds = app('Lavalite\Page\Interfaces\PageRepositoryInterface')->findWhere(config('package.page.filter-active', ['abstract'=>null, 'status'=>1]),['id'])->pluck('id')->toArray();
            
            foreach ($componentServices as $component) {
                if (!$component->placements->where('component_placement_type','Lavalite\Page\Models\Page')->whereIn('component_placement_id',$activePageIds)->count()) continue;

                if (trim($component->register_service)) {
                    try {
                        eval($component->register_service);
                    } catch (\Exception $e) {
                        app('log')->error('Component "' . $component->name . ' ' . $component->id . ' register_service: ' . $e->getMessage(), [$e]);
                    }
                }
            }
        }
        return $next($request);
    }

    public function terminate($request, $response)
    {
    }
}
