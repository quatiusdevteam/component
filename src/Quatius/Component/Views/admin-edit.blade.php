<div class="edit-component-area col-xs-12">
<div class="edit-component panel {{($component->published==1)?'panel-primary':($component->published==2?'panel-warning':'panel-default')}}" data-id="{{$component->id}}" data-type="{{$component->type}}" data-published="{{$component->published}}" data-index="{{$index}}">
  <div class="panel-heading">
  	<div class="col-xs-6">
		<div class="row">
			<div class="input-group">
				<span class="input-group-addon"><div class="move-handler"><i class="fa fa-unsorted" aria-hidden="true"></i></div></span>
                <input type="text" class="form-control component-name" placeholder="Component Name" name="components[{{$index}}][name]" value="{{$component->name}}">
            </div>
		</div>
    </div>
     <div class="col-xs-6">
     	<div class="row">
			<div class="set-position input-group">
			
				@if (user('admin.web')->hasRole('superuser') || user('admin.web')->hasPermission('component.set-position'))
         
				  <div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Position: <span class="caret"></span></button>
					<ul class="dropdown-menu">
						@foreach(config('quatius.component.positions') as $val=>$label)
						  <li><a onclick="$(this).parents('.input-group').find('> input').val($(this).attr('data-val'))" data-val="{{$val}}">{{$label}}</a></li>
						@endforeach
					</ul>
				  </div>
				  <input type="text" class="form-control" placeholder="Select a position" name="components[{{$index}}][placement][position]" value="{{$placement->position}}">
                @else
                	<span class="input-group-addon">Position: </span>
            		<input type="text" class="form-control" readonly placeholder="Select a position" name="components[{{$index}}][placement][position]" value="{{$placement->position}}">
            
            	@endif
            	
            	<span class="input-group-addon">
          		@if (user('admin.web')->hasRole('superuser') || user('admin.web')->hasPermission($component->detachable_by))
          			<button type="button" class="btn-link comp-remove"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
          		@endif
				
				@if (user('admin.web')->hasRole('superuser'))
          			<button type="button" class="btn-link" onclick="$(this).parents('.edit-component-area').toggleClass('show-options')"><i class="fa fa-gear" aria-hidden="true"></i></button>
          		@endif

				<button type="button" class="btn-link" onclick="togglePublish(this)" {{(user('admin.web')->hasRole('superuser') || user('admin.web')->hasPermission('component.set-publish'))?'':'disabled="disabled"'}}>
					<i class="indicate-publish fa fa-eye-slash" data-published="0" aria-hidden="true" title="disabled"></i>
					<i class="indicate-publish fa fa-eye" data-published="1" aria-hidden="true" title="published"></i>
					<i class="indicate-publish fa fa-bullseye" data-published="2" aria-hidden="true" title="preview"></i>
				</button>

				</span>  
				
				<span class="input-group-addon">
					<button type="button" class="btn-link" onclick="toggleCompExpand(this)">
						@if ($component->published)
						<i class="indicate-expand fa fa fa-minus" aria-hidden="true"></i>
						@else
						<i class="indicate-expand fa fa-plus" aria-hidden="true"></i>
						@endif
					</button>
				</span>
			</div>
		</div>
  	</div>
  	<div class="clearfix"></div>
  		</div>
	<div class="panel-body" {!! ($component->published)?'':'style="display:none"'!!}}>
		<div class="options">
			<div class="col-xs-4">
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon">header-color</span>
						{!! Form::select('components['.$index.'][data][header-colour]','')
						-> options([
							''=>'No Header',
							'primary'=>'Primary',
							'secondary'=>'Secondary',
							'success'=>'Success',
							'info'=>'Info',
							'warning'=>'Warning',
							'danger'=>'Danger',
							//'transparent'=>'Transparent',
							'light'=>'Light',
							'dark'=>'Dark'
						])
						->value($component->getParams('header-colour','primary')) !!}
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon">ID #</span>
						<input type="text" class="form-control" placeholder="Class" name="components[{{$index}}][data][id]" value="{{$component->getParams('id','')}}">
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon">URL</span>
						<input type="text" class="form-control" placeholder="All Url" name="components[{{$index}}][placement][url]" value="{{$placement->url}}">
					</div>
					<br>
					<div class="input-group">
						<span class="input-group-addon">Class</span>
						<input type="text" class="form-control" placeholder="Class" name="components[{{$index}}][data][class]" value="{{$component->getParams('class','')}}">
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="row">
					<div class="input-group">
						<span class="input-group-addon">Permissions</span>
						{!! Form::multiselect('components['.$index.'][data][permissions]','')
						-> options(\Litepie\User\Models\Permission::all()->pluck('name','slug')->toArray())
						->value($component->getParams('permissions',[])) !!}
					</div>
				</div>
			</div>
		</div>
  		<input type="hidden" name="components[{{$index}}][detachable_by]" value="{{$component->detachable_by}}">
        <input type="hidden" class="comp-published" name="components[{{$index}}][published]" value="{{$component->published}}">
        <input type="hidden" name="components[{{$index}}][editable_by]" value="{{$component->editable_by}}">
        <input type="hidden" name="components[{{$index}}][type]" value="{{$component->type}}">
                        
  		<input class="component-id" type="hidden" name="components[{{$index}}][id]" value="{{$component->id}}">
        <input class="placement-id" type="hidden" name="components[{{$index}}][placement][id]" value="{{$placement->id}}">

       	<input type="hidden" name="components[{{$index}}][placement][deleted]" class="delete-placement"  value="0">
                                       
    
    <?php try { ?>
		@include($component->getAdminView('edit'))
	<?php
		} catch (Exception $e) {
			app('log')->error("Eval Component Admin View: ".$e->getMessage());
		}
	?>
	
  </div>
</div>
</div>
