
<div class="row">
    <div id="component-area" class="">
    
    @foreach($placements as $placement)
     
		@include('Component::admin-view', ['placement'=>$placement, 'component'=>$placement->getComponent()])
		
    @endforeach
    </div>
    <script>
		var baseAdminCompUrl = '{{url("/admin/component")}}';
		
		var globalSlug = "{{config('quatius.component.allpages', '')}}";
		
		if (globalSlug != '' && globalSlug == $('input[name="slug"]').val()){
			jQuery("#component-area").addClass('global-page');
		}

		function toggleExpand(itm){
			var compContainer = $(itm).parents('.view-component');
			compContainer.find('.indicate-expand').removeClass('fa-minus').removeClass('fa-plus');
			
			if (compContainer.find('.panel-body:visible').length){
				compContainer.find('.indicate-expand').addClass('fa-plus');
				compContainer.find('.panel-body').slideUp(500);
			}else{
				compContainer.find('.indicate-expand').addClass('fa-minus');
				compContainer.find('.panel-body').slideDown(500);
			}
		}
		
		$('#SaveTemplateDialog').on('show.bs.modal',function(e){
			var selectComp = $(e.relatedTarget).parents('.view-component');
			$(this).attr('data-id',selectComp.data('id'));

			$('#template_name').val(selectComp.find('.component-name').text());
			$('#template_publish_init').val(2);
			$('#template_replace').val(0);
		});
    </script>
</div>

<div id="SaveTemplateDialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <h4 class="modal-title">Save As Template</h4>
		</div>
		<div class="modal-body">
			@php
			$templates = app('ComponentHandler')->findWhere(['published'=>3])->pluck('name', 'id');
			
			if ($templates->count())
				$templateList = json_decode('{"0":"-- New Template --",'.substr($templates->toJson(),1), true);
			else {
				$templateList = ["0"=>"-- New Template --"];
			}

			@endphp
			{!! Form::text('template_name')->placeholder('Enter Name')!!}
			
			{!! Form::select('selectable_by')->options([
				'component.allow-detach' => 'Admin',
				'superuser' => 'Super only'
			])!!}

			{!! Form::select('template_publish_init')->options([
				2 => 'Preview',
				1 => 'Published'
			])!!}

			<div class="clearfix"></div>
			{!! Form::select('template_replace')->options($templateList)
				->attributes(['onchange'=>"$('#template_name').val($(this).val()!=0?$('#template_replace option:selected').text():'')"])->addClass("form-control")!!}
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-warning pull-left" onclick="deleteTemplate(this)">Delete Template</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" onclick="saveTemplate(this);" data-dismiss="modal">Save As Template</button>
		</div>
	  </div>
	</div>
  </div>
