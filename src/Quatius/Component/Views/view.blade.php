

<div class="component {{$component->type}} {{$component->getParams('class','')}}" data-id="{{$component->id}}" {!! $component->getParams('id','')? 'id="'.$component->getParams('id','').'"':''!!}>
	<?php try { ?>
		@include($component->getView('view'))
	<?php
		} catch (\Exception $e) {
			app('log')->error("Eval Component View (".$component->id.")- ".$component->name.": ".$e->getMessage());
		}
	?>
</div>

@includeStyle('css/mod-component.css')
@includeStyle('js/mod-component.js')