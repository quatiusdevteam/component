<div class="col-xs-12">
<div class="view-component panel {{($component->published==1)?'panel-primary':($component->published==2?'panel-warning':'panel-default')}}" data-id="{{$component->id}}" data-published="{{$component->published}}">
		<div class="panel-heading">
			<h4><span class="component-name">{{$component->name}}</span> <button type="button" class=" btn btn-default btn-sm pull-right" onclick="toggleExpand(this)">
				@if ($component->published == 0)
				<i class="indicate-expand fa fa fa-plus" aria-hidden="true"></i>
				@else
				<i class="indicate-expand fa fa-minus" aria-hidden="true"></i>
				@endif
			</button>
			@if (user('admin.web')->hasRole('superuser'))
				<button type="button" class=" btn btn-default btn-sm pull-right" data-toggle="modal" data-target="#SaveTemplateDialog"><i class="fa fa fa-save" aria-hidden="true"></i></button> </h4>
			@endif
		</div>
		<div class="panel-body" {!! ($component->published==0)?'style="display:none"':''!!}}>
			<?php try { ?>
				@include($component->getAdminView('view'))
			<?php
				} catch (\Exception $e) {
					app('log')->error("Eval Component Admin View: ".$e->getMessage());
				}
			?>
		</div>	
	</div>
</div>
