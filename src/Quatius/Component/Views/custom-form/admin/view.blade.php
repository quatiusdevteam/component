@if ($component->getParams('show_preview','1') == '1')
<div class="row">
   <div class='col-md-12 custom-form' id="saved_view_{{$component->id}}">
	@if ($component->getData()->compiler == 'blade' || $component->getData()->compiler == 'php')
	<?php 
        $contentForm = renderBlade($component->getData()->content, array('component' => $component));
		$contentForm = preg_replace("/<form[^>]+\>/i", "", $contentForm);
		$contentForm = preg_replace("/<\\form[^>]+\>/i", "", $contentForm);
		echo $contentForm;
	?>
	@else
		{!!$component->getData()->content!!}
	@endif
   </div>
</div>

<script type="text/javascript">
	jQuery('.custom-form [data-type="custom-form"] [type="submit"]').prop('disabled',true);
	jQuery('.custom-form [data-type="custom-form"] .g-recaptcha').hide();
	jQuery('.custom-form [data-type="custom-form"] input').prop('readonly',true);
	jQuery('.custom-form [data-type="custom-form"] textarea').prop('readonly',true);

	var temp_{{$component->id}} = jQuery('#saved_view_{{$component->id}}').clone(); 
</script>
@endif

<div class="row">
   <div class='col-md-12' id="admin_view_{{$component->id}}">
	<?php 
		if ($component->getParams('admin_view',''))
			echo renderBlade($component->getParams('admin_view',''), array('component' => $component));
	?>
   </div>
</div>