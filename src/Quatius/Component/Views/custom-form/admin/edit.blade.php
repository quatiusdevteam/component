<div class="row">
	<div class='col-md-12'>
        {!! Form::select('components['.$index.'][data][compiler]')
                        -> options(config('quatius.component.types.custom-html.compilers'))
                        -> label("Compiler:")
                        -> value($component->getData()->compiler)
                        -> placeholder("Select Compiler")!!}
    	{!! Form::textarea('components['.$index.'][data][content]')
                        -> label("Content: \$component->getParams('your-variable','')")
                        -> value($component->getData()->content)
                        -> addClass('code-editor')
                        !!}
    </div>
</div>

<div class="row">
	<div class='col-md-12'>
    	{!! Form::textarea('components['.$index.'][data][validate_json]')
                        -> label("Validator (PHP):")
                        -> value($component->getData()->validate_json)
                        -> addClass('json-editor')
                        !!}
    </div>
</div>
<div class="row">
	<div class='col-md-12'>
    	{!! Form::textarea('components['.$index.'][data][submit_execute]')
                        -> label("On Submit (PHP):")
                        -> value($component->getData()->submit_execute)
                        -> addClass('code-editor')
                        !!}
    </div>
</div>
<div class="row">
	<div class='col-md-12'>
    	{!! Form::textarea('components['.$index.'][data][submit_admin_execute]')
                        -> label("On Admin Submit (PHP):")
                        -> value($component->getParams('submit_admin_execute',''))
                        -> addClass('code-editor')
                        !!}
    </div>
</div>
<div class="row">
	<div class='col-md-12'>
        {!! Form::hidden('components['.$index.'][data][show_preview]')->value(0) !!}
        {!! Form::checkbox('components['.$index.'][data][show_preview]')
            -> label("Show Preview:")
            -> value(1)->check($component->getParams('show_preview','1')=='1')
        !!}
    	{!! Form::textarea('components['.$index.'][data][admin_view]')
                        -> label("Admin View (Blade): (\$component) - \$component->getParams('your-variable','')")
                        -> value($component->getParams('admin_view',''))
                        -> addClass('code-editor')
                        !!}
    </div>
</div>
<div class="row">
    @if(!isset($adminEditing) && $component->getParams('admin_edit',''))
        <div class='col-md-6'>
            {!! Form::textarea('components['.$index.'][data][admin_edit]')
                            -> label("Admin Edit (Blade): (\$component,\$index ) -  &#123;!!Form::text('components['.\$index.'][data][your-variable]')->value(\$component->getParams('your-variable',''))!!&#125;")
                            -> value($component->getParams('admin_edit',''))
                            -> addClass('code-editor')
                            !!}
        </div>
        <div class='col-md-6'>
        <?php 
        if ($component->getParams('admin_edit','')){
            try{
                echo renderBlade($component->getParams('admin_edit',''), array('component' => $component,'index'=>$index));
            }
            catch(\Exception $e){
                echo $e->getMessage();
            }
        }
        ?>
        </div>
    @else
        <div class='col-md-12'>
            {!! Form::textarea('components['.$index.'][data][admin_edit]')
            -> label("Admin Edit (Blade): (\$component,\$index ) - &#123;!!Form::text('components['.\$index.'][data][your-variable]')->value(\$component->getParams('your-variable',''))!!&#125;")
            -> value($component->getParams('admin_edit',''))
            -> addClass('code-editor')
            !!}
        </div>
    @endif
    <div class='col-md-12'>
        {!! Form::textarea('components['.$index.'][data][admin_save]')
        -> label("Admin Save (PHP): (\$component)")
        -> value($component->getParams('admin_save',''))
        -> addClass('code-editor')
        !!}
    </div>

    <div class="col-xs-12 component_register_service" style="{{user('admin.web')->hasRole('superuser')?'':'display:none;'}}border:1px solid orange; float: left; width: 100%;">
        <p class="bg-warning">Please test global service properly, it will bring the site down.</p>
        <textarea class="code-editor" name="components[{{$index}}][register_service]" style="width:100%;">{!!$component->register_service!!}</textarea>
    </div>
</div>