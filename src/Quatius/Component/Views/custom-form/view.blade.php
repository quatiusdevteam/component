
@if ($component->getData()->compiler == 'blade' || $component->getData()->compiler == 'php')
	{!!renderBlade($component->getData()->content, array('component' => $component))!!}
@else
	{!!$component->getData()->content!!}
@endif

