@if (count($images) > 0)
	<div class="col-xs-12">
	<div class="row">
	    <div id="showcaseCarousel" class="carousel slide" data-ride="carousel">
	      <!-- Indicators -->
			@if(count($images) > 1)
				<ol class="carousel-indicators" style="text-align: center; bottom: 1px;">
				@for($s=0; $s < count($images); $s++)
					<li data-target="#showcaseCarousel" data-slide-to="{{$s}}" <?php echo $s!=0?:'class="active"'; ?>></li>
				@endfor
			   </ol>
			@endif
	      <div class="carousel-inner" role="listbox">
	      	@for($s=0; $s < count($images); $s++)
		       <div class="item <?php echo $s!=0?:"active"; ?>">
		      		@if ($placement->position == 'showcase')
						<img src="{!! URL::to('/image/xl/'.$images[$s]['efolder'] )!!}/{!! $images[$s]['file'] !!}">
					@else
						<img src="{!! URL::to('/image/lg/'.$images[$s]['efolder'] )!!}/{!! $images[$s]['file'] !!}">
					@endif	
		        </div>
	         @endfor
	      </div>
			@if(count($images) > 1)
			  <a class="left carousel-control" href="#showcaseCarousel" style="background-image: none;" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#showcaseCarousel" style="background-image: none;" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
			@endif
	    </div><!-- /.carousel -->
	</div>
	</div>
@endif