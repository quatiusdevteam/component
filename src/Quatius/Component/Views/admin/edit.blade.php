
<div class="box-header with-border">
    <h3 class="box-title">{{$component->name." - comp:".$component->id.", pos:".$placement->id}}</h3>
        <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body">
    @include('Component::admin-edit', ['placement'=>$placement, 'component'=>$component])
</div>
<div class="box-footer" >
&nbsp;
</div>