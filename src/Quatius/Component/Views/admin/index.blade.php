@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> Component <small> Manage Component</small>
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Component</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-component'>
</div>
@stop


@section('title')
Component Postitions
@stop

@section('tools')
@if (user('admin.web')->hasRole('superuser'))
    <button type="button" class="btn btn-warning btn-sm" data-action="DELETE" data-load-to='#entry-component' data-datatable='#main-list' data-href='{!! URL::to('/admin/component/clear') !!}' >
        <i class="fa fa-times-circle"></i> Remove Unlinked Components
    </button>
@endif

{{-- <h4>
<a href='#' class="label label-primary filter-role" data-role=''>All</a>
@foreach(User::roles() as $role)
<a href='#' class="label label-warning filter-role" data-role='{{ $role->name }}'>{{ ucfirst($role->name) }}</a>
@endforeach
</h4> --}}
@stop

@section('content')
<?php 
//set_time_limit(360); // 1 hours

?>
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>updated_at</th>
            <th>Name</th>
            <th>Comp ID</th>
            <th>Type</th>
            <th>URL/Page ID</th>
            <th>Placement Ids</th>
            <th>STATUS</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
@stop

@section('script')
@include('Media::admin.partials.selection-script')

<script type="text/javascript">

summernoteMediaPlugin('.html-editor');

var oTable;
$(document).ready(function(){
    oTable = $('#main-list').DataTable({
        "columns": [
            { "data": "updated_at" },
            { "data": "name" },
            { "data": "id" },
            { "data": "type" },
            { "data": "url" },
            { "data": "placement_ids" },
            { "data": "published"}
        ],
        "order": [[ 0, "desc" ]],
        serverSide: true,
        ajax: {
            url: "{{ URL::to('/admin/component') }}",
            "dataType": "json",
            "type": "GET",
            "data":{
                extra_fields : [
                    'component_placement_id','id'
                ]
            }
        },
        "columnDefs": [
        	// { "width": "20px", "targets": 0 },
			// { "width": "30px", "targets": [1] },
			// { "width": "80px", "targets": [2] },
        	// {
        	// 	"targets": [0,1],
        	// 	"createdCell": function (td, cellData, rowData, row, col) {
            		
        	// 		$(td).addClass('text-center');
        	// 	}
        	// },
        	{
                "targets": 4,
                "render": function ( data, type, row ) {
                    var data = row.url?row.url:row.component_placement_id;
                        if (row.published==3) return 'template';
                        return data?data:'null';
                }
            },
            // {
            //     "targets": 6,
            // 	"render": function ( data, type, row ) {
            //         return "";
            //     }
            // },

        	// {
            // 	"class": "set-category",
            // 	"render": function ( data, type, row ) {
            //     	var cateNames = " &nbsp;";
            //     	if (data == null || data==""){
                		
            //     	}else{
            //         	var ids = data.split(",");
                    	
            //         	var cateNames =  $('#changeCategory .category-item[data-id-raw="'+ ids[0]+'"] > label .category-name' ).text();
            //         	if (ids.length > 1){
            //             	for (var i = 1; i < ids.length; i++){
    		// 					cateNames += ", "+ $('#changeCategory .category-item[data-id-raw="'+ ids[i]+'"] > label .category-name' ).text(); 
    		// 				} 
    		// 			}
			// 		}
            // 		return '<a href="#" style="display:block" onclick="showCategoryDialog(this)">'+cateNames+'</a>';
            //     },"targets": 4
            // },
        ]
    });

    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = oTable.row( this ).data();
        $('#entry-component').load('{{URL::to('admin/component')}}/' + d.id + '/position/0');
    });

    $('.filter-role').on( 'click', function (e) {
        role = $( this ).data( "role" );

        oTable.ajax.url('{!! URL::to('/admin/user/user?role=') !!}' + role).load();
        e.preventDefault();
    });
});
</script>
@stop
@section('style')
@stop
