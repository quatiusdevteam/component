
<div class="box-header with-border">
    <h3 class="box-title">{{$component->name." - comp:".$component->id.", pos:".$placement->id}}</h3>
    <div class="box-tools pull-right">
        
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-component' data-href="{{URL::to('admin/component/'.$component->id.'/position/'.$placement->id.'/edit')}}"><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
            @if (user('admin.web')->hasRole('superuser'))
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-component' data-datatable='#main-list' data-href='' >
                    <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
                </button>
            @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body">
    @include('Component::admin-view', ['placement'=>$placement, 'component'=>$component])
</div>
<div class="box-footer" >
&nbsp;
</div>