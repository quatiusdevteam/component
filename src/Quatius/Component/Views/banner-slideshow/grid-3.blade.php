<?php $medias = Media::getLinkFrom($component)->where('pivot.grouping', 'image');?>

@for($s=0; $s < $medias->count() && $s < 3 ; $s++)
	<div class="col-xs-4 grid-3">
		<div class="row">
			@include('Component::banner-slideshow.item-view',['media'=>$medias->get($s)])
		</div>
	</div>
@endfor