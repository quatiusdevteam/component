<div class="row">
@if(!isset($component->getData()->view))
	@include($component->getView('slider'))
@else
	@include($component->getView($component->getData()->view))
@endif
</div>
<div class="clearfix"></div>