<?php 
$medias = Media::getLinkFrom($component)->where('pivot.grouping', 'image');
$grap = $component->getParams('grap','6');
?>

@if ($medias->count() > 0)
	
		<div class="col-xs-4">
			<div class="row">
			@for($s=0; $s < $medias->count()&& $s < 2; $s++)
				<div class="col-xs-12">
					<div class="row" style="padding-right: 8px; padding-{{$s==0?'bottom':'top'}}: {{$grap - 1}}px;">
						@include('Component::banner-slideshow.item-view',['media'=>$medias->get($s)])
					</div>
				</div>
			@endfor
			</div>
		</div>
		<?php
		$media = $medias->get(2);
		?>
		
		@if($media)
			<div class="col-xs-8">
				<div class="row" style="padding-left: {{$grap}}px;">
						@include('Component::banner-slideshow.item-view',['media'=>$media])
				</div>
			</div>
		@endif
@endif