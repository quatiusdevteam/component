@php
    $params = [];
    if(isset($media->pivot) && isset($media->pivot->params))
    {
        $params = json_decode($media->pivot->params, true);
        $params = $params?:[];
    }

    $isSliding =isset($isSliding)?$isSliding:false;

    $params = array_merge([
        'title'=>'', 
        'description'=>'', 
        'full_text'=>'', 
        'link'=>'',
        'extra'=>'',
        "autoplay"=>"1",
        "mute"=>"1",
        "controls"=>"0",
        "loop"=>"1",
        "valign"=>"middle",
        "caption_padding"=>"16"
        ], $params);
    $srcUrl = '';

    $vidAttr = $params["controls"] == "1" ? " controls":"";
    $vidAttr .= $params["loop"] == "1" ? " loop":"";

    $vidAttr .= $params["mute"] == "1" ? " mute":"";

    if($isSliding){
        $vidAttr .= $params["autoplay"] == "1" ? " data-autoplay":"";
    }else{
        $vidAttr .= $params["autoplay"] == "1" ? " autoplay":"";
    }
    
    if ($media->mime_type == 'image/gif')
        $srcUrl = media_url($media);
    else if(isset($resizeWidth) && isset($resizeHeight))
        $srcUrl = media_url($media, $resizeWidth, $resizeHeight, true);
    else if(isset($resizeWidth))
        $srcUrl = media_url($media, $resizeWidth);
    else 
        $srcUrl = media_url($media);
   
@endphp 
<div class="banner-item">
@if($params['link'] != '')
    <a class="link" href="{{$params['link']}}" target="{{isset($params['link_target'])?$params['link_target']:'_self'}}">
@endif	

    @if($params['title'] != '' || $params['description'] != '' || $params['full_text'] != '')
    <div class="caption caption-{{$params['valign']}}">
        {!!$params['title'] == ''?'':'<span class="title">'.$params['title'].'</span>'!!}
        {!!$params['description'] == ''?'':'<span class="description">'.$params['description'].'</span>'!!}
        {!!$params['full_text'] == ''?'':'<div class="full_text" '.($params['caption_padding']?'style="padding: '.$params['caption_padding'].'px"':'').'>'.$params['full_text'].'</div>'!!}

    </div>
    @endif	

    @if($media->mime_type == "video/mp4")
        <video {!! $vidAttr!!} width="100%" webkit-playsinline playsinline>
            <source src="{{media_url($media)}}">
        </video>
    @else
        <img class="img-responsive" {!!(isset($srcTag)?$srcTag:'src').'="'.$srcUrl.'"'!!}>
    @endif
@if($params['link'] != '')
    </a>
@endif	
</div>