<?php $medias = Media::getLinkFrom($component)->where('pivot.grouping', 'image');?>

@for($s=0; $s < $medias->count(); $s++)
	<div class="col-xs-6 grid-2">
		<div class="row">
			@include('Component::banner-slideshow.item-view',['media'=>$medias->get($s)])
		</div>
	</div>
@endfor