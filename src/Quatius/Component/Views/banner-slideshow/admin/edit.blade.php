<div class="row">
	<div class='col-md-6'>
	<div class="input-group">
      <span class="input-group-addon">View Type: </span>
          {!! Form::select('components['.$index.'][data][view]', "")
                            -> options([
                              'slider'=>'Slider --Default',
                            	'stacker'=>'stacker',
                              'grid-2'=>'Grid of 2',
                              'grid-3'=>'Grid of 3',
                              'grid-4'=>'Grid of 4',
                              'grid-2-1'=>'Grid of 2-1',
                              'grid-1-2'=>'Grid of 1-2'
                            ])
                            -> value($component->getParams('view','slider'))
                            ->attributes(['onchange'=>"hideMobileImages(this)"])
                            ->addClass('form-control')
                            -> placeholder("Views")!!}
    </div>
  	    
    </div>
</div>

<?php $medias = Media::getLinkFrom($component);?>
@include('Media::admin.selection-dialog',['mime_types' => 'image/png,image/jpeg,image/gif,video/mp4', 'allow_duplicate'=>true,'input_prefix'=>"components[".$index."][medias]", 'onSelect'=>'editImageBanner(this)','medias'=>$medias,'path'=>'banners'])

<div class="clearfix"></div>
<div class="moble-view" {!! $component->getParams('view','slider') == 'slider'? '':'style="display:none"'!!}>
<br>
  <hr>
<h4>Mobile Responsive Banners</h4>
@include('Media::admin.selection-dialog',['grouping'=>'mobile','mime_types' => 'image/png,image/jpeg,image/gif,video/mp4', 'allow_duplicate'=>true,'input_prefix'=>"components[".$index."][medias]", 'onSelect'=>'editImageBanner(this)','medias'=>$medias,'path'=>'banners/mobile'])
</div>
<?php 

$editBannerDialog = <<< EOF
<div id="edit-banner-dialog" class="modal fade " tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Set Banner Caption</h4>
      </div>
      <div class="modal-body">
        <div class="col-xs-12">
            <form class="form form-horizontal">
              <div class="form-group" style="display:none">
                <label for="captionInputTitle">Caption Title</label>
                <input name="title" class="form-control captionInputTitle" type="text" id="captionInputTitle" placeholder="Title">
              </div>
              <div class="form-group" style="display:none">
                <label for="captionInputDescription">Description</label>
                <textarea name="description" class="form-control captionInputDescription" id="captionInputDescription" placeholder="Description"></textarea> 
              </div>
              <div class="form-group">
                <label for="captionInputFullText" style="display: block" >Content Caption : <span class="pull-right">Padding : <input name="caption_padding" type="number" value="16" style="width: 50px; height: 22px;"> ,  Alignment : <select name="valign">
                        <option value="middle">Middle</option>
                        <option value="bottom">Bottom</option>
                        <option value="top">Top</option>
                    </select></label> </span>
                <textarea name="full_text" class="form-control captionInputFullText" id="inputFullText" placeholder="Content"></textarea> 
              </div>
              
              <div class="form-group">
                <label for="captionLink">Link (Open On:)</label>
                <div class="input-group">
                  <span class="input-group-addon">
                    <select name="link_target">
                        <option value="_self">Same window</option>
                        <option value="_blank">New Window</option>
                    </select>
                  </span>
                  <input name="link" class="form-control captionLink" type="text" id="captionLink" placeholder="http://">
                </div>
              </div>
              <div class="form-group video-options" style="display:">
                <h4>Video Options</h4>
                <label class="col-xs-6" for="video-autoplay"><input id="video-autoplay" onchange="if(\$(this).prop(\\'checked\\')) \$(\\'#video-mute\\').prop(\\'checked\\',true).iCheck(\\'update\\'); " name="autoplay" type="checkbox" checked value="1"> Autoplay</label>
                <label class="col-xs-6" for="video-mute"><input id="video-mute" name="mute" type="checkbox" checked value="1"> Mute</label>
                <label class="col-xs-6" for="video-loop"><input id="video-loop" name="loop" type="checkbox" checked value="1"> Loop Video</label>
                <label class="col-xs-6" for="video-control"><input id="video-control" name="controls" type="checkbox" value="1"> Show Control</label>
              </div>
            </form>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="updateImageBanner(this)">Update Changes</button>
      </div>
    </div>
  </div>
</div>
EOF;

?>

@script
<script type="text/javascript">
    $(document).ready(function(){
        $('.edit-component[data-type="banner-slideshow"] .media-selection-area').attr('data-path', $("#content").data('path')+'/images');
    });

	var componentImageEdit = null;
	var editBannerDialog = '{!!str_replace(["\n", "\t", "\r"], "", $editBannerDialog)!!}';
	function editImageBanner(itm){
		componentImageEdit = $(itm).parents('.media-tile');
		
		if ($('#edit-banner-dialog').length == 0){
			$(editBannerDialog).appendTo('body');
		}else
		{
			$('#edit-banner-dialog').find('input,textarea').not('[type="radio"]').not('[type="checkbox"]').val("");
		}
		$('#edit-banner-dialog [type="radio"]').prop('checked', false);
		$('#edit-banner-dialog [type="checkbox"]').prop('checked', false).iCheck('update');
    if (componentImageEdit.data('mime_type') == "video/mp4"){
      $('#edit-banner-dialog .video-options').show();
    }else
    {
      $('#edit-banner-dialog .video-options').hide();
    }
		$('#edit-banner-dialog').find('select')[0].selectedIndex = 0;
		initMiniEditor($('#edit-banner-dialog .captionInputFullText'),'destroy');
		try{
      if (componentImageEdit.find('.params').val()!=""){
        jQuery.parseJSON(componentImageEdit.find('.params').val());
        $.each(jQuery.parseJSON(componentImageEdit.find('.params').val()), function( key, value ) {
          if ($('#edit-banner-dialog [type="radio"][name="'+key+'"]').length>0 || $('#edit-banner-dialog [type="checkbox"][name="'+key+'"]').length>0){
            $('#edit-banner-dialog [name="'+key+'"][value="'+value+'"]').prop("checked",true).iCheck('update');
          }
          else{
            $('#edit-banner-dialog [name="'+key+'"]').val(value);
          }
        });
      }else{
        if (componentImageEdit.data('mime_type') == "video/mp4"){
          $('#edit-banner-dialog .video-options [type="checkbox"]').prop('checked', true).iCheck('update');
          $('#edit-banner-dialog .video-options [name="controls"]').prop('checked', false).iCheck('update');
        }

        $('#edit-banner-dialog [name="caption_padding"]').val(16);
      }
      
      initMiniEditor($('#edit-banner-dialog .captionInputFullText'));
		}catch(errors){
		}
		$('#edit-banner-dialog').modal('show');
	}

	function updateImageBanner(itm){
		var params = {};
		$.each($('#edit-banner-dialog form').serializeArray(), function(i, v) {
			params[v.name] = v.value;
		});
		componentImageEdit.find('.params').val(JSON.stringify(params));
		$('#edit-banner-dialog').modal('hide');
	}

  function hideMobileImages(itm){
		if($(itm).val()=='slider'){
      $(itm).parents('.edit-component').find('.moble-view').show();
    }else{
      $(itm).parents('.edit-component').find('.moble-view').hide();
    }
	}
</script>
@script