<?php 
	$medias = Media::getLinkFrom($component);
	$fullMedias = $medias->where('pivot.grouping', 'image');
	$mobileMedias = $medias->where('pivot.grouping', 'mobile');
?>
<div class="col-xs-12">
<div class="row">
    <div class="col-xs-12">
        <div class="row media-selection-list">
        @foreach($fullMedias as $media)
            @include('Media::admin.partials.tile', ['media'=>$media, 'extras'=>''])
        @endforeach
        </div>
    </div>

    @if ($component->getParams('view','slider')=='slider' && $mobileMedias->count())
    <div class="clearfix"></div>
    <br>
    <hr>
    <h4>Mobile Responsive Banners</h4>
    <div class="col-xs-12">
        <div class="row media-selection-list">
        @foreach($mobileMedias as $media)
            @include('Media::admin.partials.tile', ['media'=>$media, 'extras'=>''])
        @endforeach
        </div>
    </div>
    @endif
</div>
</div>