<?php $medias = Media::getLinkFrom($component)->where('pivot.grouping', 'image');?>

@for($s=0; $s < $medias->count() && $s < 4 ; $s++)
	<div class="col-xs-3 grid-4">
		<div class="row">
			@include('Component::banner-slideshow.item-view',['media'=>$medias->get($s)])
		</div>
	</div>
@endfor