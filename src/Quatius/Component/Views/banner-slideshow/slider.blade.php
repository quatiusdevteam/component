@includeStyle('css/slick.css')
@includeStyle('css/slick-theme.css')
@includeScript('js/slick.min.js')

<?php 
	$medias = Media::getLinkFrom($component);
	$fullMedias = $medias->where('pivot.grouping', 'image');
	$mobileMedias = $medias->where('pivot.grouping', 'mobile');
?>
@if ($medias->count())
	
<div id="carousel_{{$component->type.'_'.$placement->id}}" class="full-banner-slider">
	@foreach($fullMedias as $media)
		@include('Component::banner-slideshow.item-view',['isSliding'=>true,'srcTag'=>'data-lazy','media'=>$media])
	@endforeach
</div>

<div id="carousel_{{$component->type.'_'.$placement->id}}_mobile" class="full-banner-slider">
	@foreach($mobileMedias as $media)
		@include('Component::banner-slideshow.item-view',['isSliding'=>true,'srcTag'=>'data-lazy','media'=>$media])
	@endforeach
</div>
<script type="text/javascript">

	$(document).ready(function(){
		@if($mobileMedias->count())
			mobileComponentWidth = $(window).width();
			$( window ).resize( function(){
				if ($(this).width() != mobileComponentWidth) {
					mobileComponentWidth = $(this).width();
					switchSlider(mobileComponentWidth, "#carousel_{{$component->type.'_'.$placement->id}}", "#carousel_{{$component->type.'_'.$placement->id}}_mobile");
				}
			});
			switchSlider(mobileComponentWidth, "#carousel_{{$component->type.'_'.$placement->id}}", "#carousel_{{$component->type.'_'.$placement->id}}_mobile");
		@else
			initSlider('#carousel_{{$component->type.'_'.$placement->id}}');
		@endif
	});
</script>

@script
<script type="text/javascript">
	var mobileComponentWidth = 0;

	function switchSlider(curWidth, fullSlider, mobileSlider){
		if (curWidth < 768){
			$(fullSlider).hide();
			initSlider(mobileSlider);
			$(mobileSlider).show();
		}else{
			$(mobileSlider).hide();
			initSlider(fullSlider);
			$(fullSlider).show();
		}
	}

	function initSlider(itm){
		var jItm = $(itm);

		if (jItm.hasClass('slick-initialized')) return;

		jItm.on('init', function(event, slick, currentSlide, nextSlide){
				var activeVid = $(this).find(".slick-active video");
				if (activeVid.length) {
					if ($(this).find('.slick-slide').length == 1)
						activeVid.get(0).loop = true;

					if (activeVid.get(0).hasAttribute('data-autoplay')){
						
						if (activeVid.get(0).hasAttribute('mute')){
							activeVid.get(0).muted = true;
						}
						activeVid.get(0).play().catch(function() {
							console.log("activeVid play mute required");
						});
					}
				}
				if ($(this).find('.slick-slide').length == 1)
					$(this).find('.slick-dots').hide();
		});

		setSlickCarousel(jItm,{
				dots: true,
				speed: 1000,
				autoplay: true,
				infinite: true,
				pauseOnHover: true,
				lazyLoad: 'anticipated', // ondemand progressive anticipated
				autoplaySpeed: 5000}
		);

		jItm.each(function(){
			autoplayVideoSlide(this);
		});
	}
	</script>
	@endscript
@endif