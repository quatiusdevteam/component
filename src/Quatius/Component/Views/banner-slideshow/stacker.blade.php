<?php $medias = Media::getLinkFrom($component)->where('pivot.grouping', 'image');?>

@if ($medias->count() > 0)
	
	@for($s=0; $s < $medias->count(); $s++)
		<div class="row">
			@include('Component::banner-slideshow.item-view',['media'=>$medias->get($s)])
		</div>
	@endfor
@endif