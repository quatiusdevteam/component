<?php $medias = Media::getLinkFrom($component)->where('pivot.grouping', 'image');?>

@if ($medias->count() > 0)
	<?php 
	$grap = $component->getParams('grap','6');
	$media = $medias->get(0);
	?>
	<div class="col-xs-8">
		<div class="row" style="padding-right: {{$grap}}px;">
			@include('Component::banner-slideshow.item-view',['media'=>$medias->get(0)])
		</div>
	</div>
	<div class="col-xs-4">
		<div class="row">
		@for($s=1; $s < $medias->count()&& $s < 3; $s++)
			<div class="col-xs-12">
				<div class="row" style="padding-left: 8px; padding-{{$s==1?'bottom':'top'}}: {{$grap - 1}}px;">
						@include('Component::banner-slideshow.item-view',['media'=>$medias->get($s)])
				</div>
			</div>
		@endfor
		</div>
	</div>
@endif