<div class="row">
	<div class='col-md-12'>
        {!! Form::text('components['.$index.'][data][view]')
                        -> label("Module View:")
                        -> value($component->getData()->view)
                        -> placeholder("Module View")!!}
    </div>
</div>

<div class="row">
	<div class='col-md-12'>
    	{!! Form::textarea('components['.$index.'][data][preload_php]')
               -> label("View Composer (PHP): Variables (view, component, slug, page)")
               -> value($component->getData()->preload_php)
        !!}
    </div>
</div>