<div class="row">
	<div class='col-md-12'>
	@if (user('admin.web')->hasRole('superuser'))
  	    {!! Form::select('components['.$index.'][data][compiler]')
                        -> options(config('quatius.component.types.custom-html.compilers'))
                        -> label("Compiler:")
                        -> value($component->getData()->compiler)
                        -> placeholder("Select Compiler")!!}
  	@else
  		<input type="hidden" name="components[{{$index}}][compiler]" value="{{$component->compiler}}">

  	@endif
  	
    	{!! Form::textarea('components['.$index.'][data][content]')
                        -> label("")
                        -> id('custom_html_'.$index)
                        -> value($component->getData()->content)
                        -> addClass('html-editor')
                        -> dataPath('/custom-html')
                        !!}
    </div>
</div>
@script
<script>
    $(document).ready(function(){
        $('.edit-component[data-type="custom-html"] .html-editor').attr('data-path', $("#content").data('path'));
    });
</script>
@script