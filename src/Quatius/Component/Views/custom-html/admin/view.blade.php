<div class="row">
   <div class='col-md-12'>
	@if ($component->getData()->compiler == 'blade' || $component->getData()->compiler == 'php')
		{!! renderBlade($component->getData()->content, array('component' => $component))!!}
	@elseif($component->getData()->compiler == 'text')
		{{$component->getData()->content}}
	@else
		{!!$component->getData()->content!!}
	@endif
   </div>
</div>