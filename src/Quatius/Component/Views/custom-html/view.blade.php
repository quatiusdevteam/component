@if ($component->getData()->compiler == 'blade' || $component->getData()->compiler == 'php')
	{!! renderBlade($component->getData()->content, array('component' => $component))!!}
@elseif($component->getData()->compiler == 'text')
	{{$component->getData()->content}}
@else
	{!!$component->getData()->content!!}
@endif