
	<div class="row">
        <div id="component-area" class="editable" data-permissions="{{user('admin.web')->hasRole('superuser')?'superuser ':''}}{{user('admin.web')->hasPermission('component.set-publish')?'set-publish ':''}}{{user('admin.web')->hasPermission('component.set-url')?'set-url ':''}}{{user('admin.web')->hasPermission('component.set-class')?'set-class ':''}}{{user('admin.web')->hasPermission('component.set-test')?'set-test ':''}}">
        <?php $index = 0;?>
        @foreach($placements as $placement)
         	@include('Component::admin-edit', ['index'=>$index, 'placement'=>$placement, 'component'=>$placement->getComponent()])
    		<?php $index++;?>
        @endforeach
        </div>
   </div>
	@if (user('admin.web')->hasPermission('component.select-existing') || user('admin.web')->hasRole('superuser'))
		<hr>
		<div class="row">
      <div class='col-xs-6'>
        
			</div>
			<div class='col-xs-6'>
        <select class="form-control" onchange="checkSelectedComponent(this)" id="new_component" name="new_component">
          <option value="" disabled="disabled" selected="selected">Select Component</option>
          <optgroup label="Base" data-type="base">
            @foreach($selectableTypes as $type=>$name)
            <option value="{{$type}}">{{$name}}</option>
            @endforeach
          </optgroup>
          @if ($componentList->count())
          <optgroup label="Saved" data-type="id">
            <@foreach($componentList as $compId=>$name)
            <option value="{{$compId}}">{{$name}}</option>
            @endforeach
          </optgroup>
          @endif
          @if ($superList->count())
          <optgroup label="Super Only" data-type="id">
            <@foreach($superList as $compId=>$name)
            <option value="{{$compId}}">{{$name}}</option>
            @endforeach
          </optgroup>
          @endif
        </select>
			</div>
    	</div>
	@else
		@if($selectableTypes)
		<hr>
		<div class="row">
			<div class='col-xs-6'>
			</div>
			<div class='col-xs-6'>
				{!! Form::select('new_component')
					-> options($selectableTypes)
					-> setAttributes(['onchange' => 'addComponent(this)'])
					-> placeholder('Select Component')!!}
			</div>
		</div>
		@endif
	@endif

<script>
  var baseAdminCompUrl = '{{url("/admin/component")}}';
  $(document).ready(function() {
    
  var globalSlug = "{{config('quatius.component.allpages', '')}}";
  
  if (globalSlug != '' && globalSlug == $('input[name="slug"]').val()){
    jQuery("#component-area").addClass('global-page');
  }

  $("#component-area.editable").sortable({
    handle: ".move-handler",
    placeholder: "slide-placeholder",
    axis: "y",
    revert: 150,
    start: function(e, ui) {
      placeholderHeight = ui.item.outerHeight();
      ui.placeholder.height(placeholderHeight + 15);
      $(
        '<div class="slide-placeholder-animator" data-height="' +
          placeholderHeight +
          '"></div>'
      ).insertAfter(ui.placeholder);
    },
    change: function(event, ui) {
      ui.placeholder
        .stop()
        .height(0)
        .animate(
          {
            height: ui.item.outerHeight() + 15
          },
          300
        );

      placeholderAnimatorHeight = parseInt(
        $(".slide-placeholder-animator").attr("data-height")
      );

      $(".slide-placeholder-animator")
        .stop()
        .height(placeholderAnimatorHeight + 15)
        .animate(
          {
            height: 0
          },
          300,
          function() {
            $(this).remove();
            placeholderHeight = ui.item.outerHeight();
            $(
              '<div class="slide-placeholder-animator" data-height="' +
                placeholderHeight +
                '"></div>'
            ).insertAfter(ui.placeholder);
          }
        );
    },
    stop: function(e, ui) {
      $(".slide-placeholder-animator").remove();
    }
  });

  initComp();
});

</script>