<?php

namespace Quatius\Component\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Event;
use DB;
use Route;
use Quatius\Component\Middlewares\GlobalComponents;

class ComponentServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../Databases/migrations');

        $types = config('quatius.component.types', []);

        //Supports of older versions
        if (!isset($types['page-images']['composer']['Component::page-images.view'])) {
            $types['page-images']['composer']['Component::page-images.view'] = '
                $view->images = [];
                if ($view->placement->component_placement_type == "Lavalite\\Page\\Models\\Page"){
                    $page = app("page")->findWhere(["id"=>$view->placement->component_placement_id])->first();

    				if ($page)
    					$view->images = $page->images;
    			}
';
        }

        if (!isset($types['module-viewer']['composer']['Component::module-viewer.view'])) {
            $types['module-viewer']['composer']['Component::module-viewer.view'] = '
		        
                $component = $view->component;
    			$page = $component->getUrlData()["page"];
    			$slug = $component->getUrlData()["param"];
    			
    			eval($component->getData()->preload_php);
';
        }
        //end Support sections

        foreach ($types as $type) {
            if (!isset($type['composer'])) {
                continue;
            }

            foreach ($type['composer'] as $composer_view => $composer_script) {
                view()->composer($composer_view, function ($view) use ($composer_script) {
                    eval($composer_script);
                });
            }
        }

        view()->composer('Component::page-edit', function ($view) {

            $user = user('admin.web');
            $view->selectableTypes = [];
            foreach (config('quatius.component.types') as $type => $component) {
                if ($component['selectable'] && ($user->hasRole('superuser') || $user->hasPermission($component['detachable_by'])))
                    $view->selectableTypes[$type] = $component['name'];
            }

            $view->componentList = app('ComponentHandler')->makeModel()->wherePublished(3)->whereDetachableBy('component.allow-detach')->orderBy('updated_at', 'desc')->pluck('name', 'id');
            $view->superList = $user->hasRole('superuser')?app('ComponentHandler')->makeModel()->wherePublished(3)->where('detachable_by','<>','component.allow-detach')->orderBy('updated_at', 'desc')->pluck('name', 'id'):collect([]);
        });

        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', GlobalComponents::class);

        $this->publishes([
            realpath(__DIR__ . '/../') . '/Publishes/public' => public_path()
        ]);

        $this->publishes([
            realpath(__DIR__ . '/../') . '/Publishes/public-forced' => public_path()
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('Component', function () {
            return app('Quatius\Component\Component');
        });

        AliasLoader::getInstance()->alias('Component', 'Quatius\Component\Facades\ComponentFacade');

        $this->app->bind('Quatius\Component\Repositories\ComponentRepository', 'Quatius\Component\Repositories\Handlers\ComponentHandler');
        $this->app->bind('ComponentHandler', 'Quatius\Component\Repositories\ComponentRepository');

        Event::listen('theme.events.before', function ($theme) {
            if ($theme->getThemeName() == "admin") {
                $theme->asset()->add('codemirror_admin_style', 'packages/codemirror/lib/codemirror.css');
                $theme->asset()->add('codemirror_admin_script', 'packages/codemirror/lib/codemirror.js');
                $theme->asset()->add('codemirror_theme_style', 'packages/codemirror/theme/eclipse.css');

                $theme->asset()->add('codemirror_mode_htmlmixed', 'packages/codemirror/mode/htmlmixed/htmlmixed.js');
                $theme->asset()->add('codemirror_mode_xml', 'packages/codemirror/mode/xml/xml.js');
                $theme->asset()->add('codemirror_mode_javascript', 'packages/codemirror/mode/javascript/javascript.js');
                $theme->asset()->add('codemirror_mode_css', 'packages/codemirror/mode/css/css.js');
                $theme->asset()->add('codemirror_mode_clike', 'packages/codemirror/mode/clike/clike.js');
                $theme->asset()->add('codemirror_mode_php', 'packages/codemirror/mode/php/php.js');

                $theme->asset()->add('codemirror_addon_matchbrackets', 'packages/codemirror/addon/edit/matchbrackets.js');

                $theme->asset()->add('component_admin_style', 'css/mod-component-admin.css');
                $theme->asset()->add('component_admin_script', 'js/mod-component-admin.js');
            }
        });
        
        config()->set('theme.events.asset',function($asset)
		{
			eventFire('theme.events.asset', [$asset]);
		});

        Event::listen('theme.events.asset', function ($asset) {
            $theme = app('theme');
            if (config('quatius.component.rendered') === null) {
                if (
                    $theme->getThemeName() == "admin" ||
                    $theme->getLayoutName() == 'ajax' ||
                    $theme->getLayoutName() == 'login'
                ) {
                    return;
                }

                app('Quatius\Component\Component')->renderPositions('');
            }
        });
    }

    public function map()
    {
    }
}
