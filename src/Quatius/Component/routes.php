<?php

Route::group(array('module' => 'Component', 'middleware' => ['web'], 'namespace' => 'Quatius\Component\Controllers'), function () {
	Route::post('component/{id}/{action?}/{value?}', 'ComponentSubmitController@SubmitForm');
	Route::get('component/{id}/{action?}/{value?}', 'ComponentSubmitController@SubmitForm');
});
Route::group(['prefix' => 'admin', 'module' => 'Component', 'middleware' => ['web', 'auth:admin.web'],  'namespace' => 'Quatius\Component\Controllers'], function () {
	Route::get('component/create/{index}/{type}', 'ComponentController@createView');
	Route::get('component/edit/{component_id}/{index}', 'ComponentController@editComponent');

	Route::get('component/type/{component_type}/{action?}/{value?}', 'ComponentController@submitAdminByType');
	Route::post('component/type/{component_type}/{action?}/{value?}', 'ComponentController@submitAdminByType');

	Route::get('component/submit/{component_id}/{action?}/{value?}', 'ComponentController@submitAdminById');
	Route::post('component/submit/{component_id}/{action?}/{value?}', 'ComponentController@submitAdminById');
	Route::delete('component/submit/{component_id}/{action?}/{value?}', 'ComponentController@submitAdminById');

	Route::get('component', 'ComponentController@index');
	Route::post('component', 'ComponentController@index');

	Route::post('component/template/{comp_id}/copy/{override_id}', 'ComponentController@copyTemplate');
	Route::delete('component/template/{comp_id}', 'ComponentController@removeTemplate');

	Route::get('component/{comp_id}/position/{pos_id}/edit', 'ComponentController@edit');
	Route::get('component/{comp_id}/position/{pos_id}', 'ComponentController@show');
	
	Route::delete('component/clear', 'ComponentController@clearUnlinkedComponents');
});
