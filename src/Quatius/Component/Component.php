<?php

namespace Quatius\Component;

use Theme;

class Component
{
    public function getPlacementsByUrl($url = '', $model = null, $publishOnly = true, $isFront = false)
    {
        return app('ComponentHandler')->getPlacementsByUrl($url, $model, $publishOnly, $isFront, config('quatius.component.test_mode', false));
    }

    public function renderPositions($url = '', $extra = [])
    {

        if (config('quatius.component.rendered') === null)
            config()->set('quatius.component.rendered', $url);
        elseif (config('quatius.component.rendered') == $url) // already rendered
            return;

        $placements = $this->getPlacementsByUrl($url, isset($extra['page']) ? $extra['page'] : null, true, true);

        $groupPositions = [];

        foreach ($placements as $placement) {
            $groupPositions[$placement->position][] = $placement;
        }

        if (count($groupPositions) > 0) {
            foreach ($groupPositions as $position => $placements) {
                foreach ($placements as $placement) {
                    $component = $placement->getComponent($extra);
                    
                    if ($component->getParams('permissions',[]) && !user()->hasPermissions($component->getParams('permissions',[])))
                        continue;

                    $renderedViews = $component->renderView($placement);
                    Theme::append($position, $renderedViews);
                }
            }
        }
    }
}
